import pandas as pd

def get_titanic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def extract_title(name: str) -> str:
    # Split the name by comma and take the second part (first name with title),
    # then split by period to isolate the title.
    title = name.split(',')[1].split('.')[0].strip()
    # Define a map to standardize titles or collapse rare titles into a single category
    title_map = {
        "Mr": "Mr.",
        "Mrs": "Mrs.",
        "Miss": "Miss.",
        "Master": "Master.",
    }
    # Return the standardized title or a default title if not in the map
    return title_map.get(title, "Other")

def get_filled():
    # get df
    df = get_titanic_dataframe()
    # Extract and map the title from the Name column
    df['Title'] = df['Name'].apply(extract_title)
    # Group by title and calculate median age for each group
    median_ages = df.groupby('Title')['Age'].median().round().astype('Int64')
    # Count missing values for each group
    missing_values = df.groupby('Title')['Age'].apply(lambda x: x.isnull().sum())
    
    # Create a list of tuples as requested for specific titles
    filled_values = [(title, missing_values.get(title, 0), median_ages.get(title, None)) for title in ['Mr.', 'Mrs.', 'Miss.']]
    
    return filled_values


